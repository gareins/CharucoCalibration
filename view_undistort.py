from os import listdir
import sys

import cv2
import numpy as np

# mat = np.array([
#     [663.86, 0, 612.33],
#     [0, 667.18, 453.35],
#     [0, 0, 1]])
# 
# distort = np.array(
#         [-2.821e-01, 7.728e-02, 1.137e-04, -7.794e-04, -9.44e-03])

# mat = np.array([
#     [668.00, 0, 614.24],
#     [0, 669.35, 462.38],
#     [0, 0, 1]])
# 
# distort = np.array(
#         [-2.823e-01, 7.737e-02, 3.777e-05, -1.119e-03, -9.335e-03])

mat = np.array([
    [675.9, 0, 603.5],
    [0, 675.9, 464.7],
    [0, 0, 1]])

distort = np.array([-2.805e-01, 7.341e-02, 0, 0, -8.396e-03])

if __name__ == "__main__":
    assert(len(sys.argv) == 2)
    for filename in listdir(sys.argv[1]):
        if not filename.endswith('.png'):
            continue

        img = cv2.imread(sys.argv[1] + filename)
        undist = cv2.undistort(img, mat, distort, None, mat)

        w  = 1280 // 4 * 3
        h  = 960 // 4 * 3

        img_res = cv2.resize(img, (w, h))
        undist_res = cv2.resize(undist, (w, h))

        together = np.concatenate((img_res, undist_res), axis=1)

        cv2.imshow('untitled', together)
        cv2.waitKey(100)


