#!/bin/bash

pkg-config opencv --cflags 1>/dev/null 2>&1
if [ $? -eq 1 ]
then
  echo "Cannot find opencv"
  exit 1
fi

command -v g++ > /dev/null
gpp=$?
command -v clang > /dev/null
clang=$?

if [ $gpp -eq 0 ]
then
  CC=g++
elif [ $clang -eq 0 ]
then
  CC=clang
else
  echo "Cannot find suitable C++ compiler"
  exit 1
fi

FILE_IN="charuco_calibrate.cpp"
if [ ! -f $FILE_IN ]
then
  echo "Cannot find file: $FILE_IN"
  exit 1
fi

FILE_OUT="/tmp/charuco_calibrate_$(date +%s)"
echo "$CC $FILE_IN -o $FILE_OUT -O2 -std=c++11 $(pkg-config opencv --cflags) $(pkg-config opencv --libs)"
$CC $FILE_IN -o $FILE_OUT -O1 -std=c++11 $(pkg-config opencv --cflags) $(pkg-config opencv --libs) 

if [ $? -ne 0 ]
then
  echo "Fail compiling"
  exit 2
fi

$FILE_OUT ${@:1:99}

