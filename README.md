# Charuco calibrate

This serves for calibration of camera using charuco board.


## Preparation

You can use any charuco board for this calibration, however default values are set for included [pdf](board.pdf).
Be careful when printing to print with original sizes. This board has a width of 6 and a height of 8 squares,
side length of squares is 3cm and of aruco markers is 1.5cm. It also uses DICT\_50x50\_50 dictionary of aruco markers.
One needs to obtain multiple images of this board and provide the location of folder containing these images.

## Running

You need opencv and g++/clang installed on your Linux box (does this work on Mac and Windows's Linux subsystem?).
Then just run runme.sh bash script and that will on the fly compile the source and run it.


## How does this work

The C++ program obtains all images. First filtering we remove all files, where the number of detected corners on
charuco board is at least *-mc* (default: 7). Then every we remove frames in such a way, that every part of image contains the same amount of corners or that there is no part of image, that contains many more markers than another. *-n* frames that are left are used in OpenCV's camera calibration functions *calibrateCameraAruco* and *calibrateCameraCharuco*. On my Intel i7-7700, using 100 frames, the calibration takes about 10 minus.


## Other parameters:

The description of parameters can be obtained if the runme.sh is run without any parameters. 
A bit more on some of these parameters:

* Dictionary is from OpenCV library, to find out more just read the documentation of Aruco OpenCV module.
* *del* gives you in how many buckets is the image partitioned. Should divide width and height of images.
* *dp* are parameters that get loaded into `cv::aruco::DetectorParameters`. See `readDetectorParameters`.
* *rs* tries to find more corners on images.
* *rf* randomly filters out more images, so that we get different calibration results. Idea is to 
determine how stable the calibration process is.

## For RaspberryPi

For the project this was written for, the author used this raspistill command to obtain images:

```raspistill -w 2560 -h 1920 -fp -tl 666 -t 120000 -o out%03d.jpg```

Then, copy the images into folder *imgs* on a PC. Then, this command

```./runme.sh imgs -a=1 --out=calib.xml -n=80```

## Maybe TODO

* Obtain best rounded calibration parameters (nobody wants 15 decimal places)
