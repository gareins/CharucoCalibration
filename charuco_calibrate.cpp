/*
By downloading, copying, installing or using the software you agree to this
license. If you do not agree to this license, do not download, install,
copy or use the software.

                          License Agreement
               For Open Source Computer Vision Library
                       (3-clause BSD License)

Copyright (C) 2013, OpenCV Foundation, all rights reserved.
Third party copyrights are property of their respective owners.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

  * Neither the names of the copyright holders nor the names of the contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

This software is provided by the copyright holders and contributors "as is" and
any express or implied warranties, including, but not limited to, the implied
warranties of merchantability and fitness for a particular purpose are
disclaimed. In no event shall copyright holders or contributors be liable for
any direct, indirect, incidental, special, exemplary, or consequential damages
(including, but not limited to, procurement of substitute goods or services;
loss of use, data, or profits; or business interruption) however caused
and on any theory of liability, whether in contract, strict liability,
or tort (including negligence or otherwise) arising in any way out of
the use of this software, even if advised of the possibility of such damage.
*/

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <opencv2/aruco/charuco.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>

#define DIGITS 5

namespace {
const char* about = "Calibration using a ChArUco board\n"
                    "  Generate a video of charuco board and provide it via -v\n"
                    "  Values w, h, sl, ml and d are by default as on attached PDF";
const char* keys = "{w        | 6       | Number of squares in X direction }"
                   "{h        | 8       | Number of squares in Y direction }"
                   "{sl       | 0.03    | Square side length (in meters) }"
                   "{ml       | 0.015   | Marker side length (in meters) }"
                   "{wview    | 640     | Width of window for vieweing progress }"
                   "{d        | 0       | dictionary: DICT_4X4_50=0, DICT_4X4_100=1, "
                   "DICT_4X4_250=2,"
                   "DICT_4X4_1000=3, DICT_5X5_50=4, DICT_5X5_100=5, DICT_5X5_250=6, "
                   "DICT_5X5_1000=7, "
                   "DICT_6X6_50=8, DICT_6X6_100=9, DICT_6X6_250=10, DICT_6X6_1000=11, "
                   "DICT_7X7_50=12,"
                   "DICT_7X7_100=13, DICT_7X7_250=14, DICT_7X7_1000=15, DICT_ARUCO_ORIGINAL = "
                   "16}"
                   "{out      | out.xml | Output file with calibrated camera parameters }"
                   "{i        |         | Folder of input images }"
                   "{n        | -1      | Number of frames used for calibration, all if non "
                   "provided }"
                   "{sh       | 3       | Sharpen kernel size, if 1 disabled, only even numbers }"
                   "{rad      | 600     | Do not calibrate outsize center circle of this radius. -1 to disable}"
                   "{d5       | false   | By default we use 8 distortion parameters, if true then five}"
                   "{mc       | 7       | Minimal number of corners detected on frame }"
                   "{del      | 80      | Side length for one bucket when filtering frames }"
                   "{rs       | false   | Apply refind strategy }"
                   "{zt       | false   | Assume zero tangential distortion }"
                   "{a        |         | Fix aspect ratio (fx/fy) to this value }"
                   "{pc       | false   | Fix the principal point at the center }"
                   "{rf       | 0       | Randomize filtering of frames a bit [0-1] }";

const char* window_title = "Cahruco calibration";
const std::array<std::string, 8> allowed_extensions
    = { "bmp", "jpeg", "jpg", "png", "pbm", "pgm", "ppm", "tiff" };
}

using bucket_t = std::vector<int>;
using buckets_t = std::vector<std::vector<bucket_t> >;

double round_to_digits(double value, int digits)
{
    if (value == 0.0) // otherwise it will return 'nan' due to the log10() of zero
        return 0.0;

    double factor = pow(10.0, digits - ceil(log10(fabs(value))));
    return round(value * factor) / factor;   
}

cv::Mat round(cv::Mat mat_to_round)
{
    cv::Mat to_ret = mat_to_round.clone();
    to_ret.forEach<double>([](double &p, const int*) -> void { p = round_to_digits(p, DIGITS); });
    return to_ret;
}

/**
 */
static bool saveCameraParams(const std::string& filename, cv::Size imageSize,
    const cv::Mat& cameraMatrix, const cv::Mat& distCoeffs, double totalAvgErr)
{
    cv::FileStorage fs(filename, cv::FileStorage::WRITE);

    if (!fs.isOpened())
        return false;

    time_t tt;
    time(&tt);
    struct tm* t2 = localtime(&tt);
    char buf[1024];
    strftime(buf, sizeof(buf) - 1, "%c", t2);

    fs << "calibration_time" << buf;

    fs << "image_width" << imageSize.width;
    fs << "image_height" << imageSize.height;
    
    fs << "camera_matrix" << round(cameraMatrix);
    fs << "distortion_coefficients" << round(distCoeffs);

    fs << "avg_reprojection_error" << totalAvgErr;

    return true;
}


void print_distribution_of_corners(const buckets_t& buckets, int rows, int cols)
{
    for (int y = 0; y < rows; y++) {
        for (int x = 0; x < cols; x++) {
            std::cout << std::setw(3) << buckets[y][x].size() << " ";
        }
        std::cout << std::endl;
    }
    std::cout << "--" << std::endl;
}


int main(int argc, char* argv[])
{
    cv::CommandLineParser parser(argc, argv, keys);
    parser.about(about);
    if (argc < 2) {
        parser.printMessage();
        return 1;
    }

    // loading stuff from parser
    int squaresX = parser.get<int>("w");
    int squaresY = parser.get<int>("h");
    float squareLength = parser.get<float>("sl");
    float markerLength = parser.get<float>("ml");
    int dictionaryId = parser.get<int>("d");
    std::string outputFile = parser.get<std::string>("out");
    int delim = parser.get<int>("del");
    unsigned num_corners = parser.get<unsigned>("mc");
    int width_view = parser.get<unsigned>("wview");

    int calibrationFlags = 0;
    float aspectRatio = 1;

    if (parser.has("a")) {
        calibrationFlags |= cv::CALIB_FIX_ASPECT_RATIO;
        aspectRatio = parser.get<float>("a");
    }
    if (parser.get<bool>("zt"))
        calibrationFlags |= cv::CALIB_ZERO_TANGENT_DIST;
    if (parser.get<bool>("pc"))
        calibrationFlags |= cv::CALIB_FIX_PRINCIPAL_POINT;
    if (!parser.get<bool>("d5"))
	calibrationFlags |= CV_CALIB_RATIONAL_MODEL;
    
    // useful to detect more corners, but also makes corners less accurate
    // detectorParams->errorCorrectionRate = 1.0;

    bool refindStrategy = parser.get<bool>("rs");
    cv::String imgs_folder = parser.get<cv::String>("i");
    int num_frames = parser.get<int>("n");
    int kernel_size = parser.get<int>("sh");
    int mask_radius = parser.get<int>("rad");
    
    cv::Ptr<cv::aruco::DetectorParameters> detectorParams = cv::aruco::DetectorParameters::create();
    // TODO: as input parameters
    detectorParams->cornerRefinementMinAccuracy = 0.02;
    detectorParams->errorCorrectionRate = 0.4;
    
    // randomization
    float rand_filtering = parser.get<float>("rf");
    srand(time(NULL));

    if (!parser.check()) {
        parser.printErrors();
        return 1;
    }

    // obtain image data (raw data)
    std::cout << "Reading images..." << std::endl;
    std::vector<std::vector<char> > all_images;

    for (auto ext : allowed_extensions) {
        std::vector<cv::String> image_names;
        cv::glob(imgs_folder + "*." + ext, image_names, true);

        for (auto image : image_names) {
            if (rand() * 1.0 / RAND_MAX < rand_filtering) {
                continue;
            }

            std::ifstream input(image, std::ios::binary);
            std::vector<char> buffer(
                (std::istreambuf_iterator<char>(input)), (std::istreambuf_iterator<char>()));

            // filtering implemented quite dumbly, still should be effective
            // for small values rand_filtering
            if (buffer.size() > 0) {
                std::cout << "\rImage " << all_images.size() << " loaded" << std::flush;
                all_images.push_back(buffer);
            }
        }
    }
    std::cout << std::endl;

    if (all_images.size() == 0) {
        std::cerr << "No images found" << std::endl;
        return 1;
    }

    // getting height and width
    cv::Mat first_image = cv::imdecode(all_images[0], CV_LOAD_IMAGE_ANYDEPTH);
    int width = first_image.cols;
    int height = first_image.rows;
    CV_Assert(width % delim == 0 && height % delim == 0);
    cv::Size imgSize(width, height);
    int rows = height / delim;
    int cols = width / delim;

    // setup sharpening
    cv::Mat kernel = cv::Mat::ones(kernel_size, kernel_size, CV_32F);
    kernel *= -1;
    kernel.at<float>(kernel_size / 2, kernel_size / 2) = kernel_size * kernel_size;

    // setup circle mask
    mask_radius = mask_radius < 0 ? (width + height) : mask_radius;
    cv::Mat mask_img(height, width, CV_8U);
    mask_img *= 0;
    cv::circle(mask_img, cv::Point(width / 2, height / 2), mask_radius, 255, -1);

    // create aruco dictionary
    cv::Ptr<cv::aruco::Dictionary> dictionary
        = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));

    // create charuco board object
    cv::Ptr<cv::aruco::CharucoBoard> charucoboard = cv::aruco::CharucoBoard::create(
        squaresX, squaresY, squareLength, markerLength, dictionary);
    cv::Ptr<cv::aruco::Board> board = charucoboard.staticCast<cv::aruco::Board>();

    // data containers for each image
    std::vector<std::vector<std::vector<cv::Point2f> > > corners;
    std::vector<std::vector<int> > ids;
    std::vector<cv::Mat> images;

    // buckets for corners distribution on image
    buckets_t buckets;

    for (int i = 0; i < rows; i++) {
        buckets.push_back(std::vector<bucket_t>(cols));
    }

    int height_view = (width_view * 1.0 / width) * height;
    std::cout << "Parsing images..." << std::endl;
    for (std::vector<char>& image_data : all_images) {
        cv::Mat temp;
        cv::Mat image = cv::imdecode(image_data, CV_LOAD_IMAGE_COLOR);
        
        // sharpening
        cv::filter2D(image, image, -1, kernel, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT );
        
        // remove points farthest from center
        temp = image * 0;
        image.copyTo(temp, mask_img);
        image = temp;

        // some garbage collection
        image_data.clear();
        image_data.shrink_to_fit();

        CV_Assert(image.cols == width && image.rows == height);
        std::cout << "\rImage " << ids.size() << " parsed" << std::flush;

        std::vector<int> image_ids;
        std::vector<std::vector<cv::Point2f> > image_corners, rejected;

        // detect markers
        cv::aruco::detectMarkers(
            image, dictionary, image_corners, image_ids, detectorParams, rejected);

        // refind strategy to detect more markers
        if (refindStrategy)
            cv::aruco::refineDetectedMarkers(image, board, image_corners, image_ids, rejected);

        // interpolate charuco corners
        cv::Mat currentCharucoCorners, currentCharucoIds;

        if (image_ids.size() > 0)
            cv::aruco::interpolateCornersCharuco(image_corners, image_ids, image, charucoboard,
                currentCharucoCorners, currentCharucoIds);

        // draw results
        cv::Mat image_copy = image.clone();

        if (image_ids.size() > 0)
            cv::aruco::drawDetectedMarkers(image_copy, image_corners);

        if (currentCharucoCorners.total() > 0)
            cv::aruco::drawDetectedCornersCharuco(
                image_copy, currentCharucoCorners, currentCharucoIds);

        cv::resize(image_copy, image_copy, cv::Size(width_view, height_view));
        cv::imshow(window_title, image_copy);
        cv::waitKey(1);

        // skip if not enough corners
        if (currentCharucoCorners.total() < num_corners) {
            continue;
        }

        // storing
        int frame_num = ids.size();
        corners.push_back(image_corners);
        ids.push_back(image_ids);
        images.push_back(image);

        // collectiong corners into buckets
        for (auto pnt : image_corners) {
            int x = (pnt[0].x + pnt[1].x) / 2;
            int y = (pnt[0].y + pnt[1].y) / 2;
            buckets[y / delim][x / delim].push_back(frame_num);
        }
    }

    std::cout << std::endl;
    cv::destroyWindow(window_title);

    if (ids.size() < 1) {
        std::cerr << "Not enough captures for calibration" << std::endl;
        return 1;
    }

    std::cout << "Distribution of corners: " << std::endl;
    print_distribution_of_corners(buckets, rows, cols);

    // filter images down to num_frames
    if (num_frames != -1 && num_frames < ids.size()) {
        int idx = corners.size();

        // holding swaps of images, only keeping first num_frames
        // images in id_changes
        std::vector<int> id_changes(idx);
        for (int i = 0; i < idx; i++) {
            id_changes[i] = i;
        }

        std::cout << "Filtering... " << std::endl;
        for (; idx > num_frames; idx--) {
            // sum of number of corners in buckets where this image has corners
            std::vector<int> board_sum(idx, 0);
            std::vector<int> board_len(idx, 0);

            for (int y = 0; y < rows; y++) {
                for (int x = 0; x < cols; x++) {
                    int num = buckets[y][x].size();

                    for (int counter : buckets[y][x]) {
                        board_len.at(counter) += 1;
                        board_sum.at(counter) += num;
                    }
                }
            }

            // finding image, where sum/len if highest (most corners of
            // the board are where there are already many corners)
            float max = 0;
            int max_idx = 0;

            for (int i = 0; i < idx; i++) {
                float maybe_new_max = board_sum[i] * 1.0 / board_len[i];

                if (maybe_new_max > max) {
                    max = maybe_new_max;
                    max_idx = i;
                }
            }

            id_changes[max_idx] = idx - 1;

            // fixing buckets data structure to handle removal of image
            // with the most corners
            for (int y = 0; y < rows; y++) {
                for (int x = 0; x < cols; x++) {
                    auto& arr = buckets[y][x];

                    do {
                        auto max_idx_ptr = find(arr.begin(), arr.end(), max_idx);

                        if (max_idx_ptr != arr.end()) {
                            arr.erase(max_idx_ptr);
                        } else {
                            break;
                        }
                    } while (true);

                    do {
                        auto max_idx_ptr = find(arr.begin(), arr.end(), idx - 1);

                        if (max_idx_ptr != arr.end()) {
                            *max_idx_ptr = max_idx;
                        } else {
                            break;
                        }
                    } while (true);
                }
            }
        }

        // sanity check: no duplicated frames
        id_changes.resize(num_frames);
        sort(id_changes.begin(), id_changes.end());
        auto duplicated = unique(id_changes.begin(), id_changes.end());
        assert(duplicated == id_changes.end());

        // copy of corners, ids and imgs
        auto tmp_aco = corners;
        auto tmp_aid = ids;
        auto tmp_aim = images;
        corners.clear();
        ids.clear();
        images.clear();

        // filtering corners, ids and imgs as set in filtering process
        for (int i = 0; i < num_frames; i++) {
            int idx = id_changes[i];
            corners.push_back(tmp_aco[idx]);
            ids.push_back(tmp_aid[idx]);
            images.push_back(tmp_aim[idx]);
        }

        std::cout << "Filtered distribution of corners: " << std::endl;
        print_distribution_of_corners(buckets, rows, cols);
    }

    // some garbage collection --
    all_images.clear();
    all_images.shrink_to_fit();

    corners.shrink_to_fit();
    ids.shrink_to_fit();
    images.shrink_to_fit();

    buckets.clear();
    buckets.shrink_to_fit();
    // --------------------------

    cv::Mat cameraMatrix, distCoeffs;
    std::vector<cv::Mat> rvecs, tvecs;
    double repError;

    if (calibrationFlags & cv::CALIB_FIX_ASPECT_RATIO) {
        cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
        cameraMatrix.at<double>(0, 0) = aspectRatio;
    }

    // prepare data for calibration
    std::vector<std::vector<cv::Point2f> > allCornersConcatenated;
    std::vector<int> allIdsConcatenated;
    std::vector<int> markerCounterPerFrame;
    markerCounterPerFrame.reserve(corners.size());

    for (unsigned int i = 0; i < corners.size(); i++) {
        markerCounterPerFrame.push_back((int)corners[i].size());

        for (unsigned int j = 0; j < corners[i].size(); j++) {
            allCornersConcatenated.push_back(corners[i][j]);
            allIdsConcatenated.push_back(ids[i][j]);
        }
    }

    std::cout << "Calibrating with " << corners.size() << " images" << std::endl;

    // calibrate camera using aruco markers
    double arucoRepErr;
    arucoRepErr = cv::aruco::calibrateCameraAruco(allCornersConcatenated, allIdsConcatenated,
        markerCounterPerFrame, board, imgSize, cameraMatrix, distCoeffs, cv::noArray(),
        cv::noArray(), calibrationFlags);

    std::cout << "Rep Error Aruco (approximate): " << arucoRepErr << std::endl;

    // prepare data for charuco calibration
    int nFrames = (int)corners.size();
    std::vector<cv::Mat> allCharucoCorners;
    std::vector<cv::Mat> allCharucoIds;
    allCharucoCorners.reserve(nFrames);
    allCharucoIds.reserve(nFrames);

    for (int i = 0; i < nFrames; i++) {
        // interpolate using camera parameters
        cv::Mat currentCharucoCorners, currentCharucoIds;
        cv::aruco::interpolateCornersCharuco(corners[i], ids[i], images[i], charucoboard,
            currentCharucoCorners, currentCharucoIds, cameraMatrix, distCoeffs);

        allCharucoCorners.push_back(currentCharucoCorners);
        allCharucoIds.push_back(currentCharucoIds);
    }

    if (allCharucoCorners.size() < 4) {
        std::cerr << "Not enough corners for calibration" << std::endl;
        return 1;
    }

    // calibrate camera using charuco
    repError = cv::aruco::calibrateCameraCharuco(allCharucoCorners, allCharucoIds, charucoboard,
        imgSize, cameraMatrix, distCoeffs, rvecs, tvecs, calibrationFlags);

    bool saveOk = saveCameraParams(
        outputFile, imgSize, cameraMatrix, distCoeffs, repError);

    if (!saveOk) {
        std::cerr << "Cannot save output file" << std::endl;
        return 1;
    }

    // report
    std::cout << "Rep Error: " << repError << std::endl;
    std::cout << "Calibration saved to " << outputFile << std::endl;
    return 0;
}
